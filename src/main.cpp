#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Ticker.h>
#include <AsyncMqttClient.h>
#include <ArduinoJson.h>
#include <uptime.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>   
#include <Ticker.h>

#define WIFI_SSID "GerHuei"
#define WIFI_PASSWORD "sofia2019"

#define MQTT_HOST IPAddress(192, 168, 1, 149)
#define MQTT_PORT 1883

// Instancia a la clase Ticker
Ticker ticker;
// Pin LED azul
byte pinLed = D4;

const String node = "node1";
const int sensor_pin = A0;  /* Connect Soil moisture analog sensor pin to A0 of NodeMCU */
long wait_time = 60 * 1000; // 1 minuto

AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

String topicConf = node + String("/conf");
String topicStatus = node + String("/status");

WiFiManagerParameter custom_mqtt_server("server", "mqtt server", MQTT_HOST.toString().c_str(), 40);

boolean isInt(String number){
  for (int i = 0;(unsigned) i < number.length(); i++)
  {
    if(!isDigit(number.charAt(i))){
      return false;
    }
  }
  return true;
}

void parpadeoLed(){
  // Cambiar de estado el LED
  byte estado = digitalRead(pinLed);
  digitalWrite(pinLed, !estado);
}

void saveConfigCallback () {
 Serial.println("Debería guardar la configuración");
 //shouldSaveConfig = true;
}

void configModeCallback (WiFiManager *myWiFiManager) {
 Serial.println("Modo de configuración ingresado");
 Serial.println(WiFi.softAPIP());
 
 Serial.println(myWiFiManager->getConfigPortalSSID());
}

void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  mqttClient.setWill(topicStatus.c_str(),0,true,"{\"node\":\"node1\",\"status\":\"OFFLINE\",\"sensors\":[\"SOIL_MOISTURE\"],\"uptime\":-1}");
  mqttClient.connect();
}

void connectToWifi() {
  Serial.println("Connecting to Wi-Fi...");
  //WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  pinMode(pinLed, OUTPUT);
  ticker.attach(0.2, parpadeoLed);

  WiFiManager wifiManager;
  //wifiManager.resetSettings();
  wifiManager.setAPCallback(configModeCallback);
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.autoConnect("FarmIOT_Node1");
  //WiFi.disconnect(true);
  //wifiManager.resetSettings();
  ticker.detach();
  digitalWrite(pinLed, HIGH);
  
}

void onWifiConnect(const WiFiEventStationModeGotIP& event) {
  Serial.println("Connected to Wi-Fi.");
  connectToMqtt();
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected& event) {
  Serial.println("Disconnected from Wi-Fi.");
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once(2, connectToWifi);
}

void publishStatus(){
  uptime::calculateUptime();
  String status = String();
  DynamicJsonDocument doc(1024);
  doc["node"] = node;
  doc["status"] = "ONLINE";
  doc["sensors"][0] = "SOIL_MOISTURE";
  doc["uptime"] = uptime::getMinutes();
  serializeJson(doc,status);
  mqttClient.publish(topicStatus.c_str(), 0, true, status.c_str());
}

void onMqttConnect(bool sessionPresent) {
  Serial.println("Connected to MQTT.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);
  uint16_t packetIdSub = mqttClient.subscribe(topicConf.c_str(), 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub);
  publishStatus();
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("Disconnected from MQTT.");

  if (WiFi.isConnected()) {
    mqttReconnectTimer.once(2, connectToMqtt);
  }
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
  Serial.println("Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);

  Serial.print("  qos: ");
  Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId) {
  Serial.println("Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  Serial.println("Publish received.");
  Serial.print("  topic: ");
  Serial.println(topic);
  Serial.print("  len: ");
  Serial.println(len);
  Serial.print("  index: ");
  Serial.println(index);
  Serial.print("  total: ");
  Serial.println(total);
  Serial.print("  payload: ");
  Serial.println(payload);
  if(String(topic) == topicConf){
    String config = String(payload).substring(0,len);
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, config);
    if(error){
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.f_str());
    }else{
      Serial.print("Config: ");
      Serial.println(config);
      long tryWaitTime = doc["wait_time"];
      if(tryWaitTime > 0) { 
        wait_time = tryWaitTime;
      }
      Serial.print("Set wait time to: ");
      Serial.println(wait_time);
    }
    
  }
}

void onMqttPublish(uint16_t packetId) {
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);
  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onUnsubscribe(onMqttUnsubscribe);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.onPublish(onMqttPublish);
  delay(3000);
  connectToWifi();
  connectToMqtt();
}

void publishMoist(){
  float moisture_percentage;

  moisture_percentage = ( 100.00 - ( (analogRead(sensor_pin)/1023.00) * 100.00 ) );

  Serial.print("Soil Moisture(in Percentage) = ");
  Serial.print(moisture_percentage);
  Serial.println("%");

  String send_moisture = String();
  String topic =node + String("/read/soil_moist");

  DynamicJsonDocument doc(1024);
  doc["sensor"] = "SOIL_MOISTURE";
  doc["node"] = node;
  doc["data"]["type"] = "float";
  doc["data"]["value"] = moisture_percentage;

  serializeJson(doc,send_moisture);

  uint16_t packetIdPub1 = mqttClient.publish(topic.c_str(), 1, false, send_moisture.c_str());
  Serial.print("Publishing at QoS 2, packetId: ");
  Serial.println(packetIdPub1);
}

void loop() {
  publishMoist();
  delay(wait_time);
  publishStatus();
}